//
// Available modules include (this is not a complete list):
// var Scene = require('Scene');
// var Textures = require('Textures');
// var Materials = require('Materials');
// var FaceTracking = require('FaceTracking');
// var Animation = require('Animation');
// var Reactive = require('Reactive');
//
// Example script
//
// Loading required modules
// var Scene = require('Scene');
// var FaceTracking = require('FaceTracking');
//
// Binding an object's property to a value provided by the face tracker
// Scene.root.child('object0').transform.rotationY = FaceTracking.face(0).transform.rotationX;
//
// If you want to log objects, use the Diagnostics module.
// var Diagnostics = require('Diagnostics');
// Diagnostics.log(Scene.root);

const Networking = require('Networking');
const CameraShare = require('CameraShare');
const Patches = require('Patches');
const Diagnostics = require('Diagnostics');
const Reactive = require('Reactive');
const Time = require('Time');
const Scene = require('Scene');

let area = Reactive.val(1);

let macAddress = CameraShare.arguments['mac'];
if (!macAddress) {
    macAddress = 'c:2c:54:26:e9:5b';
}
const url = `https://catchjunction.ngrok.io/get_area?mac=${macAddress}`;
const request = {
    method: 'POST',
    body: JSON.stringify({mac: macAddress}),
    headers: {'Content-type': 'application/json; charset=UTF-8'}
}
Time.ms.interval(3000).subscribe(function() {
    Networking.fetch(url, request)
    .then(function(response) {
        if (response.status != 200) throw Exception;
        return response.json();
    })
    .then(function(json) {
        Diagnostics.log(json);
        if (json.success) {
            area = json.area;
        } else {
            area = 0;
        }
        Patches.setScalarValue('area', area);
    })
    .catch(err => Diagnostics.log(err));
});