
from botocore.session import Session
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, UnicodeSetAttribute, NumberAttribute,
    NumberSetAttribute, BooleanAttribute, UTCDateTimeAttribute,
    MapAttribute, ListAttribute, JSONAttribute
)

localhost = 'http://localhost:3000'

try:
    from config import CONST
    stage = CONST.env.stage
except Exception:
    stage = 'dev'

is_dev = False
if stage != 'production':
    is_dev = True


def to_dict(obj):
    rval = {}
    for key in obj.attribute_values:
        if isinstance(obj, MapAttribute):
            value = obj[key]
        else:
            value = obj.__getattribute__(key)
        if isinstance(value, set):
            value = list(value)
        if hasattr(value, 'attribute_values'):
            rval[key] = to_dict(value)
        else:
            rval[key] = value
    return rval


class BaseModel(Model):
    @classmethod
    def _conditional_operator_check(cls, conditional_operator):
        pass

    def to_dict(self):
        return to_dict(self)


class Area(BaseModel):

    class Meta:
        table_name = 'hackjunction-macs'
        region = Session().get_config_variable('region')
        write_capacity_units = 5
        read_capacity_units = 5
        if is_dev:
            host = localhost

    id = UnicodeAttribute(hash_key=True)
    address_list = UnicodeSetAttribute(default=set())


class User(BaseModel):

    class Meta:
        table_name = 'hackjunction-users'
        region = Session().get_config_variable('region')
        write_capacity_units = 5
        read_capacity_units = 5
        if is_dev:
            host = localhost

    id = UnicodeAttribute(hash_key=True)
    catched = UnicodeSetAttribute(default=set())


if not User.exists():
    User.create_table(wait=True)
if not Area.exists():
    Area.create_table(wait=True)
