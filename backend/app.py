# -*- coding: utf-8 -*-
import os
import logging
import db
import random
from flask import Flask, request, jsonify
from pynamodb.exceptions import DoesNotExist

app = Flask(__name__)

logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

MAC = 'c:2c:54:26:e9:5b'


# API Endpoints


@app.route('/meraki', methods=['POST'])
def meraki_event():
    data = request.json.get('data')
    tags = data.get('apTags')
    area_id = ''
    for tag in tags:
        if tag.startswith('area'):
            area_id = tag
            break
    try:
        area = db.Area.get(area_id)
    except Exception as ex:
        if isinstance(ex, DoesNotExist):
            area = db.Area(area_id)
        else:
            return 500
    observations = data.get('observations')
    macs = [o.get('clientMac') for o in observations if o.get('clientMac')]
    area.address_list = macs
    area.save()
    return 'ok'


@app.route('/get_area', methods=['POST'])
def check_mac():
    mac = request.json.get('mac')
    if mac == 'unknown_mac':
        result = {
            'success': False,
            'area': random.randint(1, 3)
        }
    else:
        current_area = None
        for area in db.Area.scan():
            for _mac in area.address_list:
                if mac in _mac:
                    current_area = area.id
                    break
        area_id = 0
        if current_area == 'area-hall':
            area_id = 1
        if current_area == 'area-cisco':
            area_id = 2
        if current_area == 'area-nokia':
            area_id = 3
        if current_area:
            result = {
                'success': True,
                'area': area_id
            }
        else:
            result = {
                'success': False
            }
    print(result)
    return jsonify(result)


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
