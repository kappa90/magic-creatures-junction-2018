package com.magicthedatagathering.junction2018;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class MainScreen extends AppCompatActivity implements View.OnClickListener {

    private String macAddress;
    private Button btnClick;
    private String cameraUrl = "fb://inspirationscamera?effectID=763555207315207&cameraShareArguments=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_screen);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        btnClick = (Button) findViewById(R.id.cameraButton) ;
        btnClick.setOnClickListener(this);

        //findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
        macAddress = getMacAddr();
        final TextView textViewToChange = (TextView) findViewById(R.id.macAddress);
        textViewToChange.setText(getMacAddr());
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public void onClick(View v) {
        if (v == btnClick) {
            JSONObject payload = new JSONObject();
            macAddress = "test_mac_address";
            try {
                payload.put("mac", macAddress);
            }
            catch (JSONException e) {}
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(cameraUrl + payload));
            startActivity(browserIntent);
        }
        return;

    }
}
